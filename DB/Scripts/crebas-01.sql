/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2005                    */
/* Created on:     18.12.2016 18:03:38                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Game') and o.name = 'FK_GAME_GAMER_WIN_PLAYER')
alter table Game
   drop constraint FK_GAME_GAMER_WIN_PLAYER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Move') and o.name = 'FK_MOVE_GAME_MOVE_GAME')
alter table Move
   drop constraint FK_MOVE_GAME_MOVE_GAME
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Move') and o.name = 'FK_MOVE_PLAYER_MO_PLAYER')
alter table Move
   drop constraint FK_MOVE_PLAYER_MO_PLAYER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Player_Game') and o.name = 'FK_PLAYER_G_PLAYER_GA_PLAYER')
alter table Player_Game
   drop constraint FK_PLAYER_G_PLAYER_GA_PLAYER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Player_Game') and o.name = 'FK_PLAYER_G_PLAYER_GA_GAME')
alter table Player_Game
   drop constraint FK_PLAYER_G_PLAYER_GA_GAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Game')
            and   name  = 'Gamer_Winner_FK'
            and   indid > 0
            and   indid < 255)
   drop index Game.Gamer_Winner_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Game')
            and   type = 'U')
   drop table Game
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Move')
            and   name  = 'Game_Move_FK'
            and   indid > 0
            and   indid < 255)
   drop index Move.Game_Move_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Move')
            and   name  = 'Player_Move_FK'
            and   indid > 0
            and   indid < 255)
   drop index Move.Player_Move_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Move')
            and   type = 'U')
   drop table Move
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Player')
            and   type = 'U')
   drop table Player
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Player_Game')
            and   name  = 'Player_Game2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Player_Game.Player_Game2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Player_Game')
            and   name  = 'Player_Game_FK'
            and   indid > 0
            and   indid < 255)
   drop index Player_Game.Player_Game_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Player_Game')
            and   type = 'U')
   drop table Player_Game
go

/*==============================================================*/
/* Table: Game                                                  */
/*==============================================================*/
create table Game (
   IdGame               int                  identity,
   IdPlayer             int                  null,
   GameType             varchar(16)          not null,
   StartDate            datetime             not null,
   EndDate              datetime             null,
   GameStatus           varchar(16)          not null,
   GameState            varchar(8000)        null,
   constraint PK_GAME primary key nonclustered (IdGame)
)
go

/*==============================================================*/
/* Index: Gamer_Winner_FK                                       */
/*==============================================================*/
create index Gamer_Winner_FK on Game (
IdPlayer ASC
)
go

/*==============================================================*/
/* Table: Move                                                  */
/*==============================================================*/
create table Move (
   IdMove               int                  identity,
   IdGame               int                  not null,
   IdPlayer             int                  not null,
   MoveTo               varchar(16)          not null,
   MoveFrom             varchar(16)          null,
   MovingObject         varchar(32)          null,
   constraint PK_MOVE primary key nonclustered (IdMove)
)
go

/*==============================================================*/
/* Index: Player_Move_FK                                        */
/*==============================================================*/
create index Player_Move_FK on Move (
IdPlayer ASC
)
go

/*==============================================================*/
/* Index: Game_Move_FK                                          */
/*==============================================================*/
create index Game_Move_FK on Move (
IdGame ASC
)
go

/*==============================================================*/
/* Table: Player                                                */
/*==============================================================*/
create table Player (
   IdPlayer             int                  identity,
   Username             varchar(32)          not null,
   Password             varchar(32)          not null,
   IsOnline             bit                  null,
   constraint PK_PLAYER primary key nonclustered (IdPlayer)
)
go

/*==============================================================*/
/* Table: Player_Game                                           */
/*==============================================================*/
create table Player_Game (
   IdPlayer             int                  not null,
   IdGame               int                  not null,
   constraint PK_PLAYER_GAME primary key (IdPlayer, IdGame)
)
go

/*==============================================================*/
/* Index: Player_Game_FK                                        */
/*==============================================================*/
create index Player_Game_FK on Player_Game (
IdPlayer ASC
)
go

/*==============================================================*/
/* Index: Player_Game2_FK                                       */
/*==============================================================*/
create index Player_Game2_FK on Player_Game (
IdGame ASC
)
go

alter table Game
   add constraint FK_GAME_GAMER_WIN_PLAYER foreign key (IdPlayer)
      references Player (IdPlayer)
go

alter table Move
   add constraint FK_MOVE_GAME_MOVE_GAME foreign key (IdGame)
      references Game (IdGame)
go

alter table Move
   add constraint FK_MOVE_PLAYER_MO_PLAYER foreign key (IdPlayer)
      references Player (IdPlayer)
go

alter table Player_Game
   add constraint FK_PLAYER_G_PLAYER_GA_PLAYER foreign key (IdPlayer)
      references Player (IdPlayer)
go

alter table Player_Game
   add constraint FK_PLAYER_G_PLAYER_GA_GAME foreign key (IdGame)
      references Game (IdGame)
go

