﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:17
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(4);
			InitGameEntityMappings();
			InitMoveEntityMappings();
			InitPlayerEntityMappings();
			InitPlayerGameEntityMappings();
		}

		/// <summary>Inits GameEntity's mappings</summary>
		private void InitGameEntityMappings()
		{
			this.AddElementMapping("GameEntity", @"GameCenter", @"dbo", "Game", 7);
			this.AddElementFieldMapping("GameEntity", "EndDate", "EndDate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("GameEntity", "GameState", "GameState", true, "VarChar", 8000, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GameEntity", "GameStatus", "GameStatus", false, "VarChar", 16, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GameEntity", "GameType", "GameType", false, "VarChar", 16, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GameEntity", "IdGame", "IdGame", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GameEntity", "IdPlayer", "IdPlayer", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GameEntity", "StartDate", "StartDate", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MoveEntity's mappings</summary>
		private void InitMoveEntityMappings()
		{
			this.AddElementMapping("MoveEntity", @"GameCenter", @"dbo", "Move", 6);
			this.AddElementFieldMapping("MoveEntity", "IdGame", "IdGame", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MoveEntity", "IdMove", "IdMove", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MoveEntity", "IdPlayer", "IdPlayer", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MoveEntity", "MoveFrom", "MoveFrom", true, "VarChar", 16, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("MoveEntity", "MoveTo", "MoveTo", false, "VarChar", 16, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("MoveEntity", "MovingObject", "MovingObject", true, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 5);
		}

		/// <summary>Inits PlayerEntity's mappings</summary>
		private void InitPlayerEntityMappings()
		{
			this.AddElementMapping("PlayerEntity", @"GameCenter", @"dbo", "Player", 4);
			this.AddElementFieldMapping("PlayerEntity", "IdPlayer", "IdPlayer", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PlayerEntity", "IsOnline", "IsOnline", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("PlayerEntity", "Password", "Password", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PlayerEntity", "Username", "Username", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits PlayerGameEntity's mappings</summary>
		private void InitPlayerGameEntityMappings()
		{
			this.AddElementMapping("PlayerGameEntity", @"GameCenter", @"dbo", "Player_Game", 2);
			this.AddElementFieldMapping("PlayerGameEntity", "IdGame", "IdGame", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PlayerGameEntity", "IdPlayer", "IdPlayer", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
		}

	}
}
