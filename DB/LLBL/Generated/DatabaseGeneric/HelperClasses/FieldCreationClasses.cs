﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:17
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using GameCenterLLBL.FactoryClasses;
using GameCenterLLBL;

namespace GameCenterLLBL.HelperClasses
{
	/// <summary>Field Creation Class for entity GameEntity</summary>
	public partial class GameFields
	{
		/// <summary>Creates a new GameEntity.EndDate field instance</summary>
		public static EntityField2 EndDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.EndDate);}
		}
		/// <summary>Creates a new GameEntity.GameState field instance</summary>
		public static EntityField2 GameState
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.GameState);}
		}
		/// <summary>Creates a new GameEntity.GameStatus field instance</summary>
		public static EntityField2 GameStatus
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.GameStatus);}
		}
		/// <summary>Creates a new GameEntity.GameType field instance</summary>
		public static EntityField2 GameType
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.GameType);}
		}
		/// <summary>Creates a new GameEntity.IdGame field instance</summary>
		public static EntityField2 IdGame
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.IdGame);}
		}
		/// <summary>Creates a new GameEntity.IdPlayer field instance</summary>
		public static EntityField2 IdPlayer
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.IdPlayer);}
		}
		/// <summary>Creates a new GameEntity.StartDate field instance</summary>
		public static EntityField2 StartDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.StartDate);}
		}
	}

	/// <summary>Field Creation Class for entity MoveEntity</summary>
	public partial class MoveFields
	{
		/// <summary>Creates a new MoveEntity.IdGame field instance</summary>
		public static EntityField2 IdGame
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.IdGame);}
		}
		/// <summary>Creates a new MoveEntity.IdMove field instance</summary>
		public static EntityField2 IdMove
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.IdMove);}
		}
		/// <summary>Creates a new MoveEntity.IdPlayer field instance</summary>
		public static EntityField2 IdPlayer
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.IdPlayer);}
		}
		/// <summary>Creates a new MoveEntity.MoveFrom field instance</summary>
		public static EntityField2 MoveFrom
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.MoveFrom);}
		}
		/// <summary>Creates a new MoveEntity.MoveTo field instance</summary>
		public static EntityField2 MoveTo
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.MoveTo);}
		}
		/// <summary>Creates a new MoveEntity.MovingObject field instance</summary>
		public static EntityField2 MovingObject
		{
			get { return (EntityField2)EntityFieldFactory.Create(MoveFieldIndex.MovingObject);}
		}
	}

	/// <summary>Field Creation Class for entity PlayerEntity</summary>
	public partial class PlayerFields
	{
		/// <summary>Creates a new PlayerEntity.IdPlayer field instance</summary>
		public static EntityField2 IdPlayer
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.IdPlayer);}
		}
		/// <summary>Creates a new PlayerEntity.IsOnline field instance</summary>
		public static EntityField2 IsOnline
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.IsOnline);}
		}
		/// <summary>Creates a new PlayerEntity.Password field instance</summary>
		public static EntityField2 Password
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.Password);}
		}
		/// <summary>Creates a new PlayerEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.Username);}
		}
	}

	/// <summary>Field Creation Class for entity PlayerGameEntity</summary>
	public partial class PlayerGameFields
	{
		/// <summary>Creates a new PlayerGameEntity.IdGame field instance</summary>
		public static EntityField2 IdGame
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerGameFieldIndex.IdGame);}
		}
		/// <summary>Creates a new PlayerGameEntity.IdPlayer field instance</summary>
		public static EntityField2 IdPlayer
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerGameFieldIndex.IdPlayer);}
		}
	}
	

}