﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using GameCenterLLBL;
using GameCenterLLBL.FactoryClasses;
using GameCenterLLBL.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Player. </summary>
	public partial class PlayerRelations
	{
		/// <summary>CTor</summary>
		public PlayerRelations()
		{
		}

		/// <summary>Gets all relations of the PlayerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GameEntityUsingIdPlayer);
			toReturn.Add(this.MoveEntityUsingIdPlayer);
			toReturn.Add(this.PlayerGameEntityUsingIdPlayer);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PlayerEntity and GameEntity over the 1:n relation they have, using the relation between the fields:
		/// Player.IdPlayer - Game.IdPlayer
		/// </summary>
		public virtual IEntityRelation GameEntityUsingIdPlayer
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Games" , true);
				relation.AddEntityFieldPair(PlayerFields.IdPlayer, GameFields.IdPlayer);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PlayerEntity and MoveEntity over the 1:n relation they have, using the relation between the fields:
		/// Player.IdPlayer - Move.IdPlayer
		/// </summary>
		public virtual IEntityRelation MoveEntityUsingIdPlayer
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Moves" , true);
				relation.AddEntityFieldPair(PlayerFields.IdPlayer, MoveFields.IdPlayer);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MoveEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PlayerEntity and PlayerGameEntity over the 1:n relation they have, using the relation between the fields:
		/// Player.IdPlayer - PlayerGame.IdPlayer
		/// </summary>
		public virtual IEntityRelation PlayerGameEntityUsingIdPlayer
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PlayerGames" , true);
				relation.AddEntityFieldPair(PlayerFields.IdPlayer, PlayerGameFields.IdPlayer);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerGameEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPlayerRelations
	{
		internal static readonly IEntityRelation GameEntityUsingIdPlayerStatic = new PlayerRelations().GameEntityUsingIdPlayer;
		internal static readonly IEntityRelation MoveEntityUsingIdPlayerStatic = new PlayerRelations().MoveEntityUsingIdPlayer;
		internal static readonly IEntityRelation PlayerGameEntityUsingIdPlayerStatic = new PlayerRelations().PlayerGameEntityUsingIdPlayer;

		/// <summary>CTor</summary>
		static StaticPlayerRelations()
		{
		}
	}
}
