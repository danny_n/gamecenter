﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using GameCenterLLBL;
using GameCenterLLBL.HelperClasses;
using GameCenterLLBL.FactoryClasses;
using GameCenterLLBL.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Game'.<br/><br/></summary>
	[Serializable]
	public partial class GameEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<MoveEntity> _moves;
		private EntityCollection<PlayerGameEntity> _playerGames;
		private PlayerEntity _player;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Player</summary>
			public static readonly string Player = "Player";
			/// <summary>Member name Moves</summary>
			public static readonly string Moves = "Moves";
			/// <summary>Member name PlayerGames</summary>
			public static readonly string PlayerGames = "PlayerGames";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GameEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public GameEntity():base("GameEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public GameEntity(IEntityFields2 fields):base("GameEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this GameEntity</param>
		public GameEntity(IValidator validator):base("GameEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="idGame">PK value for Game which data should be fetched into this Game object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GameEntity(System.Int32 idGame):base("GameEntity")
		{
			InitClassEmpty(null, null);
			this.IdGame = idGame;
		}

		/// <summary> CTor</summary>
		/// <param name="idGame">PK value for Game which data should be fetched into this Game object</param>
		/// <param name="validator">The custom validator object for this GameEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GameEntity(System.Int32 idGame, IValidator validator):base("GameEntity")
		{
			InitClassEmpty(validator, null);
			this.IdGame = idGame;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected GameEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_moves = (EntityCollection<MoveEntity>)info.GetValue("_moves", typeof(EntityCollection<MoveEntity>));
				_playerGames = (EntityCollection<PlayerGameEntity>)info.GetValue("_playerGames", typeof(EntityCollection<PlayerGameEntity>));
				_player = (PlayerEntity)info.GetValue("_player", typeof(PlayerEntity));
				if(_player!=null)
				{
					_player.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GameFieldIndex)fieldIndex)
			{
				case GameFieldIndex.IdPlayer:
					DesetupSyncPlayer(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Player":
					this.Player = (PlayerEntity)entity;
					break;
				case "Moves":
					this.Moves.Add((MoveEntity)entity);
					break;
				case "PlayerGames":
					this.PlayerGames.Add((PlayerGameEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Player":
					toReturn.Add(Relations.PlayerEntityUsingIdPlayer);
					break;
				case "Moves":
					toReturn.Add(Relations.MoveEntityUsingIdGame);
					break;
				case "PlayerGames":
					toReturn.Add(Relations.PlayerGameEntityUsingIdGame);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Player":
					SetupSyncPlayer(relatedEntity);
					break;
				case "Moves":
					this.Moves.Add((MoveEntity)relatedEntity);
					break;
				case "PlayerGames":
					this.PlayerGames.Add((PlayerGameEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Player":
					DesetupSyncPlayer(false, true);
					break;
				case "Moves":
					this.PerformRelatedEntityRemoval(this.Moves, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PlayerGames":
					this.PerformRelatedEntityRemoval(this.PlayerGames, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_player!=null)
			{
				toReturn.Add(_player);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Moves);
			toReturn.Add(this.PlayerGames);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_moves", ((_moves!=null) && (_moves.Count>0) && !this.MarkedForDeletion)?_moves:null);
				info.AddValue("_playerGames", ((_playerGames!=null) && (_playerGames.Count>0) && !this.MarkedForDeletion)?_playerGames:null);
				info.AddValue("_player", (!this.MarkedForDeletion?_player:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GameRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Move' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMoves()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(MoveFields.IdGame, null, ComparisonOperator.Equal, this.IdGame));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PlayerGame' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPlayerGames()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(PlayerGameFields.IdGame, null, ComparisonOperator.Equal, this.IdGame));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Player' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPlayer()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(PlayerFields.IdPlayer, null, ComparisonOperator.Equal, this.IdPlayer));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._moves);
			collectionsQueue.Enqueue(this._playerGames);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._moves = (EntityCollection<MoveEntity>) collectionsQueue.Dequeue();
			this._playerGames = (EntityCollection<PlayerGameEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._moves != null);
			toReturn |=(this._playerGames != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<MoveEntity>(EntityFactoryCache2.GetEntityFactory(typeof(MoveEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<PlayerGameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PlayerGameEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Player", _player);
			toReturn.Add("Moves", _moves);
			toReturn.Add("PlayerGames", _playerGames);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdGame", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdPlayer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartDate", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _player</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPlayer(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _player, new PropertyChangedEventHandler( OnPlayerPropertyChanged ), "Player", GameCenterLLBL.RelationClasses.StaticGameRelations.PlayerEntityUsingIdPlayerStatic, true, signalRelatedEntity, "Games", resetFKFields, new int[] { (int)GameFieldIndex.IdPlayer } );
			_player = null;
		}

		/// <summary> setups the sync logic for member _player</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPlayer(IEntityCore relatedEntity)
		{
			if(_player!=relatedEntity)
			{
				DesetupSyncPlayer(true, true);
				_player = (PlayerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _player, new PropertyChangedEventHandler( OnPlayerPropertyChanged ), "Player", GameCenterLLBL.RelationClasses.StaticGameRelations.PlayerEntityUsingIdPlayerStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPlayerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this GameEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GameRelations Relations
		{
			get	{ return new GameRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Move' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMoves
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<MoveEntity>(EntityFactoryCache2.GetEntityFactory(typeof(MoveEntityFactory))), (IEntityRelation)GetRelationsForField("Moves")[0], (int)GameCenterLLBL.EntityType.GameEntity, (int)GameCenterLLBL.EntityType.MoveEntity, 0, null, null, null, null, "Moves", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PlayerGame' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPlayerGames
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<PlayerGameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PlayerGameEntityFactory))), (IEntityRelation)GetRelationsForField("PlayerGames")[0], (int)GameCenterLLBL.EntityType.GameEntity, (int)GameCenterLLBL.EntityType.PlayerGameEntity, 0, null, null, null, null, "PlayerGames", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Player' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPlayer
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(PlayerEntityFactory))),	(IEntityRelation)GetRelationsForField("Player")[0], (int)GameCenterLLBL.EntityType.GameEntity, (int)GameCenterLLBL.EntityType.PlayerEntity, 0, null, null, null, null, "Player", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EndDate property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."EndDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GameFieldIndex.EndDate, false); }
			set	{ SetValue((int)GameFieldIndex.EndDate, value); }
		}

		/// <summary> The GameState property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."GameState"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 8000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GameState
		{
			get { return (System.String)GetValue((int)GameFieldIndex.GameState, true); }
			set	{ SetValue((int)GameFieldIndex.GameState, value); }
		}

		/// <summary> The GameStatus property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."GameStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String GameStatus
		{
			get { return (System.String)GetValue((int)GameFieldIndex.GameStatus, true); }
			set	{ SetValue((int)GameFieldIndex.GameStatus, value); }
		}

		/// <summary> The GameType property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."GameType"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String GameType
		{
			get { return (System.String)GetValue((int)GameFieldIndex.GameType, true); }
			set	{ SetValue((int)GameFieldIndex.GameType, value); }
		}

		/// <summary> The IdGame property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."IdGame"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 IdGame
		{
			get { return (System.Int32)GetValue((int)GameFieldIndex.IdGame, true); }
			set	{ SetValue((int)GameFieldIndex.IdGame, value); }
		}

		/// <summary> The IdPlayer property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."IdPlayer"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdPlayer
		{
			get { return (Nullable<System.Int32>)GetValue((int)GameFieldIndex.IdPlayer, false); }
			set	{ SetValue((int)GameFieldIndex.IdPlayer, value); }
		}

		/// <summary> The StartDate property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Game"."StartDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime StartDate
		{
			get { return (System.DateTime)GetValue((int)GameFieldIndex.StartDate, true); }
			set	{ SetValue((int)GameFieldIndex.StartDate, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'MoveEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MoveEntity))]
		public virtual EntityCollection<MoveEntity> Moves
		{
			get { return GetOrCreateEntityCollection<MoveEntity, MoveEntityFactory>("Game", true, false, ref _moves);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'PlayerGameEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PlayerGameEntity))]
		public virtual EntityCollection<PlayerGameEntity> PlayerGames
		{
			get { return GetOrCreateEntityCollection<PlayerGameEntity, PlayerGameEntityFactory>("Game", true, false, ref _playerGames);	}
		}

		/// <summary> Gets / sets related entity of type 'PlayerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PlayerEntity Player
		{
			get	{ return _player; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPlayer(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Games", "Player", _player, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the GameCenterLLBL.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)GameCenterLLBL.EntityType.GameEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
