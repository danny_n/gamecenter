﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameCenter.WebGUI.Models
{
    public class Player
    {
        public string Username { get; set; }
        public int IdPlayer { get; set; }
        public bool Active { get; set; }
    }
}