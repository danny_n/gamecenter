﻿using GameCenter.Common.Classes.Base;
using GameCenter.Logic.Ships;
using GameCenter.WebGUI.Models;
using GameCenter.WebGUI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GameCenter.WebGUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// Render Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var claims = identity.Claims;
            var username = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
            var idplayer = claims.FirstOrDefault(x => x.Type == "http://example.org/claims/simplecustomclaim").Value;

            return View(new LoginViewModel { Username = username, IdPlayer = Convert.ToInt32(idplayer) });
        }


        /// <summary>
        /// Render About page
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Render Contact page
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Process move of player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="idplayer"></param>
        /// <param name="idgame"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ProcessMove(int x, int y, int idplayer, int idgame)
        {
            var move = new ShipsMove();
            move.IdGame = idgame;
            move.IdPlayer = idplayer;
            move.MoveTo = $"{(char)(x + 'A')}-{y + 1}";
            move.GameType = Common.Enums.GameType.SHIPS;

            //var moveResult = new MvcToServiceConnector().MakeMove(move);
            var moveResult = new MvcToServiceConnector().MakeShipsMove(move);

            return Json(moveResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Initialize new game
        /// </summary>
        /// <param name="id"></param>
        /// <param name="boardJson"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult StartNewGame(int id, string boardJson)
        {
            int[][] board = null;
            using (var ms = new System.IO.MemoryStream())
            {
                using (var writer = new System.IO.StreamWriter(ms))
                {
                    writer.Write(boardJson);
                    writer.Flush();
                    ms.Position = 0;
                    var ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(int[][]));
                    board = (int[][])ser.ReadObject(ms);
                }
            }

            if(board == null)
                return Json(-1, JsonRequestBehavior.AllowGet);

            // other Id, not players
            var gameBoard = new ShipsGameBoard(0, board);
            gameBoard.Ships = new List<Ship>()
            {
                new Ship { Position = new Cell { X = 'D', Y = 1 }, Type = ShipType.CRUISER, IsHorizontal = true },
                new Ship { Position = new Cell { X = 'G', Y = 4 }, Type = ShipType.SUBMARINE, IsHorizontal = true },
                new Ship { Position = new Cell { X = 'A', Y = 7 }, Type = ShipType.BATTLESHIP, IsHorizontal = false },
                new Ship { Position = new Cell { X = 'H', Y = 6 }, Type = ShipType.DESTROYER, IsHorizontal = true },
            };

            var gs = new ShipsGameState();
            gs.Board_P1 = new ShipsGameBoard(3, null);
            gs.Board_P2 = gameBoard;
            gs.GameType = Common.Enums.GameType.SHIPS;

            GameStateBase gsBase = gs as GameStateBase;
            var gameId = new MvcToServiceConnector().CreateShipsGame(gs);
            return Json(gameId, JsonRequestBehavior.AllowGet);
        }
    }
}