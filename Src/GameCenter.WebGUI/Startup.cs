﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameCenter.WebGUI.Startup))]
namespace GameCenter.WebGUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
