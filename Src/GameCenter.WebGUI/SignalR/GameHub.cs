﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using GameCenter.WebGUI.Models;
using System.Linq;
using GameCenter.Common.Utils;

namespace GameCenter.WebGUI.SignalR
{
    public class GameHub : Hub
    {
        private const string ALL = "ALL";

        /// <summary>
        /// Event raised when new player connects
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            var username = Context.QueryString["username"];
            var idplayer = Context.QueryString["idplayer"].ConvertToInt();
            
            AllPlayers.Add(new Player { Username = username, IdPlayer = idplayer, Active = true });

            JoinGroup(username, idplayer);

            return base.OnConnected();
        }

        /// <summary>
        /// Event raised when player disconnects
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            var username = Context.QueryString["username"];
            var idplayer = Context.QueryString["idplayer"].ConvertToInt();

            AllPlayers.Remove(username);

            LeaveGroup(username, idplayer);

            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// Adding new player to list and notifying other players
        /// </summary>
        /// <param name="username"></param>
        /// <param name="idplayer"></param>
        /// <returns></returns>
        public async Task JoinGroup(string username, int idplayer)
        {
            await Groups.Add(Context.ConnectionId, ALL);
            await Groups.Add(Context.ConnectionId, username);

            Clients.All.broadcastLogin(username, idplayer);
            Clients.Group(username).showConnectedPlayers(AllPlayers.GetAllOthers(username));
        }

        /// <summary>
        /// Removing player from list and notifying other players
        /// </summary>
        /// <param name="username"></param>
        /// <param name="idplayer"></param>
        /// <returns></returns>
        public async Task LeaveGroup(string username, int idplayer)
        {
            Clients.All.broadcastLogout(username, idplayer);

            await Groups.Remove(Context.ConnectionId, ALL);
            await Groups.Remove(Context.ConnectionId, username);

        }
        
        /// <summary>
        /// Challenging another player to start new game
        /// </summary>
        /// <param name="username"></param>
        public void ChallengePlayer(string username)
        {
            // from
            var fromUsername = Context.QueryString["username"];

            Clients.Group(username).challenge(fromUsername);
        }

        /// <summary>
        /// Responding to challenge
        /// </summary>
        /// <param name="username"></param>
        /// <param name="idplayer"></param>
        /// <param name="value"></param>
        public void RespondToChallenge(string username, int idplayer, string value)
        {
            // from
            var fromUsername = Context.QueryString["username"];
            var fromIdplayer = Context.QueryString["idplayer"].ConvertToInt();

            Clients.Group(username).respond(fromUsername, fromIdplayer);
        }

        /// <summary>
        /// Static class representing list of connected players.
        /// </summary>
        static class AllPlayers
        {
            private static List<Player> list;

            /// <summary>
            /// Add new connected player
            /// </summary>
            /// <param name="p"></param>
            public static void Add(Player p)
            {
                if (list == null)
                    list = new List<Player>();
                list.Add(p);
            }

            /// <summary>
            /// Remove leaving player
            /// </summary>
            /// <param name="username"></param>
            public static void Remove(string username)
            {
                if (list == null)
                    return;

                list.Remove(list.FirstOrDefault(p => p.Username == username));
            }

            /// <summary>
            /// Retrivieng list of online players, excluding player requesting the list
            /// </summary>
            /// <param name="username"></param>
            /// <returns></returns>
            public static List<Player> GetAllOthers(string username)
            {
                if (list == null)
                    return new List<Player>();

                return list.Where(x => x.Username != username).ToList();
            }
        }
    }
}