﻿using System;
using GameCenter.Common.Classes.Base;
using GameCenter.Common.Classes.Service;
using GameCenter.Logic.Ships;
using GameCenter.Common.Enums;

namespace GameCenter.WebGUI.Service
{
    public class MvcToServiceConnector : ServiceConnectorBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Send request to web service for player login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override LoginResponse Login(string userName, string password)
        {
            try
            {
                var loginRequest = new LoginRequest() { Username = userName, Password = password };

                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                log.Info("LOGIN OK");
                return serviceClient.Login(loginRequest);
            }
            catch (Exception e)
            {
                log.Error("Error while calling Service.Login", e);
                return new LoginResponse(Common.Enums.ResponseStatus.LOGIN_ERROR, e.Message);
            }
        }

        /// <summary>
        /// Send request to webservice for player registration
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override RegisterResponse Register(string userName, string password)
        {
            try
            {
                var registerRequest = new RegisterRequest() { Username = userName, Password = password };

                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                log.Info("REGISTER OK");
                return serviceClient.Register(registerRequest);
            }
            catch (Exception e)
            {
                log.Error("Error while calling Service.Register", e);
                return new RegisterResponse(Common.Enums.ResponseStatus.REGISTER_ERROR, e.Message);
            }
        }

        /// <summary>
        /// Send request to webservice to create new game
        /// </summary>
        /// <param name="initialGameState"></param>
        /// <returns></returns>
        // TODO fix serialization problem
        public override int CreateGame(GameStateBase initialGameState)
        {
            try
            {
                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                var newGameId = serviceClient.CreateGame(initialGameState);

                return newGameId;
            }
            catch (Exception e)
            {
                log.Error("Error calling Service.CreateGame", e);
                return -1;
            }
        }

        /// <summary>
        /// Send request to webservice to create new game of Ships (temporary - need to fix serialization error for base classes)
        /// </summary>
        /// <param name="initialGameState"></param>
        /// <returns></returns>
        public int CreateShipsGame(ShipsGameState initialGameState)
        {
            try
            {
                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                var newGameId = serviceClient.CreateShipsGame(initialGameState);

                return newGameId;
            }
            catch (Exception e)
            {
                log.Error("Error calling Service.CreateGame", e);
                return -1;
            }
        }

        /// <summary>
        /// Send request to webservice to make a move in a game
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public override MoveResponse MakeMove(MoveBase move)
        {
            try
            {
                var moveRequest = new ShipsMove();

                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                return serviceClient.MakeMove(move);
            }
            catch (Exception e)
            {
                log.Error("Error while calling Service.MakeMove");
                return new MoveResponse(Common.Enums.ResponseStatus.MOVE_ERROR, e.Message);
            }
        }

        /// <summary>
        /// Send request to webservice to make a move in a game of Ships (temporary - need to fix serialization error for base classes)
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public MoveResult MakeShipsMove(ShipsMove move)
        {
            try
            {
                var serviceClient = new GameCenterServiceReference.GCServiceClient();
                return serviceClient.MakeShipsMovew(move);
            }
            catch (Exception e)
            {
                log.Error("Error calling Service.MakeMove", e);
                return MoveResult.ERROR;
                //return new MoveResponse { Message = e.Message, Status = ResponseStatus.MOVE_ERROR };
            }
        }
    }
}