﻿$(document).ready(function () {
    var connectedPlayers = [];
    var gameHub = $.connection.gameHub;

    var username = $("#Username").val();
    var idplayer = $("#IdPlayer").val();

    /* server callbacks*/
    gameHub.client.broadcastLogin = function (_username, _idplayer) {
        if (username === _username && idplayer == _idplayer)
            return;

        connectedPlayers = connectedPlayers.filter(function (el) {
            return el.Username !== _username && el.IdPlayer != _idplayer;
        });

        connectedPlayers.push({ 'Username': _username, 'IdPlayer': +_idplayer, 'active': true });
        updatePlayerList();
    };

    gameHub.client.broadcastLogout = function (_username, _idplayer) {
        if (username === _username && idplayer == _idplayer)
            return;

        connectedPlayers = connectedPlayers.filter(function (el) {
            return el.Username !== _username && el.IdPlayer != _idplayer;
        });
        updatePlayerList();
    };

    gameHub.client.showConnectedPlayers = function (players) {
        connectedPlayers = players;
        updatePlayerList()
    }

    gameHub.client.challenge = function (fromName) {
        $("#chalFrom").text(fromName);
    };
    /* server callbacks*/

    /* server methods*/
    $("#challBtn").click(function () {
        var u = $("#challUsername").val();
        gameHub.server.challengePlayer(u)
            .done(function () {
                console.log("challenge sent");
            })
            .fail(function () {
                console.log("challenge sent fail");
            });
    });

    $("#respBtnY").click(function () {
        gameHub.server.respondToChallenge($("#challUsername").val(), "yes")
            .done(function () {
                console.log("responde sent");
            })
            .fail(function () {
                console.log("responde sent fail");
            });
    });

    $("#respBtnN").click(function () {
        gameHub.server.respondToChallenge($("#challUsername").val(), "no")
            .done(function () {
                console.log("responde sent");
            })
            .fail(function () {
                console.log("responde sent fail");
            });
    });
    /* server methods*/

    $.connection.hub.qs = { 'username': username, 'idplayer': idplayer };
    $.connection.hub.start().done(function () {
        console.log("SignalR started");
    });

    $("#startBtn").click(function () {
        var myJsonString = JSON.stringify(gameBoard);
        $.getJSON("/Home/StartNewGame", { id: idplayer, boardJson: myJsonString },
            function (data) {
                if (data > 0) {
                    currentGameId = data;
                    $("#gameboard").show();
                    $("#moveresult").text("Game started (IdGame: " + data + ")");
                    $("#idgame").text(data);
                }
                else {
                    $("#moveresult").text("Unable to start new game");
                }
            }
        );
    });

    $("li.playerItem").click(function (event) {
        console.log(e.text());
        $("#challUsername").val();
    });

    $("li.playerItem").on('click', (function (event) {
        console.log(e.text());
        $("#challUsername").val();
    }));

    function updatePlayerList() {
        var cList = $('ul#playersList')
        cList.empty();
        $.each(connectedPlayers, function (i) {
            var li = $('<li/>')
                .addClass('ui-menu-item')
                .addClass('playerItem')
                .attr('role', 'menuitem')
                .text(connectedPlayers[i].Username)
                .appendTo(cList);
        });
    }
});