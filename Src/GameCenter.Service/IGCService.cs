﻿using GameCenter.Common.Classes.Base;
using GameCenter.Common.Classes.Service;
using GameCenter.Common.Enums;
using GameCenter.Logic.Ships;
using System.ServiceModel;

namespace GameCenter.Service
{
    [ServiceContract]
    public interface IGCService
    {
        [OperationContract]
        void TestService();

        [OperationContract]
        RegisterResponse Register(RegisterRequest request);

        [OperationContract]
        LoginResponse Login(LoginRequest request);

        [OperationContract]
        int CreateGame(GameStateBase initialGameState);

        // TODO need to be remove after fixing serialization issue
        [OperationContract]
        int CreateShipsGame(ShipsGameState initialGameState);

        [OperationContract]
        MoveResponse MakeMove(MoveBase move);

        // TODO need to be remove after fixing serialization issue
        [OperationContract]
        MoveResult MakeShipsMovew(ShipsMove move);
    }
}
