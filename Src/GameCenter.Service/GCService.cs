﻿using System;
using System.Linq;
using GameCenter.Common.Enums;
using GameCenterLLBL.DatabaseSpecific;
using GameCenterLLBL.EntityClasses;
using GameCenterLLBL.Linq;
using GameCenter.Common.Classes.Base;
using GameCenter.Common.Classes.Service;
using System.Runtime.Serialization.Json;
using GameCenter.Logic.Ships;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace GameCenter.Service
{
    [KnownType(typeof(ShipsGameState))]
    [ServiceKnownType(typeof(ShipsGameState))]
    public class GCService : IGCService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Testing method
        /// </summary>
        public void TestService()
        {

            //var t = new Test { Id = 1, Name = "Danny" };
            //MemoryStream stream1 = new MemoryStream();
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Test));
            //ser.WriteObject(stream1, t);
            //stream1.Position = 0;
            //StreamReader sr = new StreamReader(stream1);
            //var output = sr.ReadToEnd();

            //var ms = new MemoryStream();
            //var writer = new StreamWriter(ms);
            //writer.Write(output);
            //writer.Flush();
            //ms.Position = 0;
            //Test p2 = (Test)ser.ReadObject(ms);


            //var gs = new ShipsGameState();
            //gs.IdGame = 1;
            //gs.GameType = GameType.SHIPS;
            //
            //gs.Board_P1 = new ShipsGameBoard();
            //gs.Board_P1.IdPlayer = 3;
            //gs.IdGame = 1;
            //gs.Board_P1.Cells = new System.Collections.Generic.List<Logic.Base.Cell>() { new Logic.Base.Cell() { X = 'A', Y = 1, IsHit = false, IsWater = false } };
            //gs.Board_P1.Ships = new System.Collections.Generic.List<Ship>() { new Ship { HitCount = 0, IsDestroyed = false, IsHorizontal = true, Position = new Logic.Base.Cell { X = 'A', Y = '1' }, Type = ShipType.SUBAMRINE } };
            //
            //gs.Board_P2 = new ShipsGameBoard();
            //gs.Board_P2.IdPlayer = 5;
            //gs.IdGame = 1;
            //gs.Board_P2.Cells = new System.Collections.Generic.List<Logic.Base.Cell>() { new Logic.Base.Cell() { X = 'A', Y = 1, IsHit = false, IsWater = false } };
            //gs.Board_P2.Ships = new System.Collections.Generic.List<Ship>() { new Ship { HitCount = 0, IsDestroyed = false, IsHorizontal = true, Position = new Logic.Base.Cell { X = 'A', Y = '1' }, Type = ShipType.SUBAMRINE } };
            //
            //SaveGameState(gs);
            //
            //var r = LoadGameState(1);
            //
            //var g = CreateGame(gs);

            var move = new ShipsMove { IdGame = 3, GameType = GameType.SHIPS, IdPlayer = 3, MoveTo = "A-1" };
            var resMove = MakeMove(move);
        }

        /// <summary>
        /// Testing class for serialization and deserialization
        /// </summary>
        [DataContract]
        class Test
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public int Id { get; set; }

        }

        /// <summary>
        /// Method for registering new user (Player)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RegisterResponse Register(RegisterRequest request)
        {
            try
            {
                using (var adapter = new DataAccessAdapter())
                {
                    var metadata = new LinqMetaData(adapter);

                    if (metadata.Player.Any(p => p.Username == request.Username))
                        return new RegisterResponse(ResponseStatus.REGISTER_FAILED, "User allready exists") { IdPlayer = -1 };

                    var newPlayer = new PlayerEntity();
                    newPlayer.Username = request.Username;
                    newPlayer.Password = request.Password;

                    adapter.SaveEntity(newPlayer, refetchAfterSave: true, recurse: true);
                    adapter.Commit();
                    newPlayer = metadata.Player.Single(p => p.Username == request.Username);

                    log.Info($"User '{newPlayer.Username}' successufll registerd");
                    return new RegisterResponse(ResponseStatus.REGISTER_SUCCESS, newPlayer.IdPlayer);
                }
            }
            catch (Exception e)
            {
                log.Error("Error while Register", e);
                return new RegisterResponse(ResponseStatus.REGISTER_ERROR, e.Message);
            }
        }

        /// <summary>
        /// Method for user login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public LoginResponse Login(LoginRequest request)
        {
            try
            {
                using (var adapter = new DataAccessAdapter())
                {
                    log.Info("OK");

                    var metadata = new LinqMetaData(adapter);
                    var player = metadata.Player.FirstOrDefault(p => p.Username == request.Username);

                    if (player == null)
                        return new LoginResponse(ResponseStatus.LOGIN_FAILED, -1);

                    if (player.Password == request.Password)
                        return new LoginResponse(ResponseStatus.LOGIN_SUCCESS, player.IdPlayer);
                    else
                        return new LoginResponse(ResponseStatus.LOGIN_FAILED, -1);

                }
            }
            catch (Exception e)
            {
                log.Error("Error while Login", e);
                return new LoginResponse(ResponseStatus.LOGIN_ERROR, e.Message);
            }
        }

        /// <summary>
        /// Creates new Game (generally for all possible game types)
        /// </summary>
        /// <param name="initialGameState"></param>
        /// <returns>IdGame of created Game</returns>
        public int CreateGame(GameStateBase initialGameState)
        {
            try
            {
                using (var adapter = new DataAccessAdapter())
                {
                    var newGame = new GameEntity();
                    newGame.StartDate = DateTime.UtcNow;
                    newGame.GameStatus = "NEW";
                    newGame.GameType = initialGameState.GameType.ToString();

                    adapter.SaveEntity(newGame, refetchAfterSave: true);

                    initialGameState.IdGame = newGame.IdGame;
                    newGame.GameState = SerializeGameStateToJson(initialGameState);

                    adapter.SaveEntity(newGame, refetchAfterSave: true);

                    return newGame.IdGame;
                }
            }
            catch (Exception e)
            {
                log.Error("Error while creating new game", e);
                return -1;
            }
        }

        // TODO need to be remove after fixing serialization issue 
        /// <summary>
        /// Creates new SHIPS game
        /// </summary>
        /// <param name="initialGameState"></param>
        /// <returns></returns>
        public int CreateShipsGame(ShipsGameState initialGameState)
        {
            return CreateGame(initialGameState);
        }

        // TODO need to be remove after fixing serialization issue
        /// <summary>
        /// Method applies players move and calculates result for SHIPS game.
        /// All neccessary data are also stored into the database
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public MoveResult MakeShipsMovew(ShipsMove move)
        {
            //return MakeMove(move).MoveResult.MoveResult;
            return MakeMove(move).MoveResult.MoveResult;
        }

        /// <summary>
        /// Method applies players move and calculates result.
        /// All neccessary data are also stored into the database
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public MoveResponse MakeMove(MoveBase move)
        {
            MoveResponse response = new MoveResponse(ResponseStatus.MOVE_SUCCESS);
            try
            {
                var gameState = LoadGameState(move.IdGame);

                switch (move.GameType)
                {
                    case GameType.SHIPS:
                        var moveRes = ((ShipsGameState)gameState).MakeMove(move);
                        response.MoveResult = moveRes;
                        break;
                }

                SaveGameState(response.MoveResult.CurrentState);

                using (var adapter = new DataAccessAdapter())
                {
                    var newMove = new MoveEntity();
                    newMove.IdGame = move.IdGame;
                    newMove.IdPlayer = move.IdPlayer;
                    newMove.MoveTo = move.MoveTo;

                    adapter.SaveEntity(newMove);
                }
            }
            catch (Exception e)
            {
                log.Error("Error while MakeMove", e);
                response.Status = ResponseStatus.MOVE_ERROR;
                response.Message = e.Message;
            }

            return response;
        }

        /// <summary>
        /// Load GameState from database as JSON string and deserialize it into the appropiate object
        /// </summary>
        /// <param name="idGame"></param>
        /// <returns></returns>
        private GameStateBase LoadGameState(int idGame)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var metadata = new LinqMetaData(adapter);
                var game = metadata.Game.FirstOrDefault(x => x.IdGame == idGame);

                if (game == null)
                    return null;

                var data = game.GameState;

                var result = DeserializeGameStateFromJson(data, game.GameType.ToString());
                
                return result;
            }
        }

        /// <summary>
        /// Saves GameState object into database as serialized JSON string
        /// </summary>
        /// <param name="gameState"></param>
        private void SaveGameState(GameStateBase gameState)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var metadata = new LinqMetaData(adapter);
                var game = metadata.Game.FirstOrDefault(x => x.IdGame == gameState.IdGame);

                if (game == null)
                    game = new GameEntity() { GameType = gameState.GameType.ToString(), StartDate = DateTime.UtcNow, GameStatus = "OPEN" };

                game.GameState = SerializeGameStateToJson(gameState);
                adapter.SaveEntity(game);
            }
        }

        /// <summary>
        /// Serializes GameState object into JSON string
        /// </summary>
        /// <param name="gameState"></param>
        /// <returns></returns>
        private string SerializeGameStateToJson(GameStateBase gameState)
        {
            try
            {
                DataContractJsonSerializer ser = null;
                switch (gameState.GameType)
                {
                    case GameType.SHIPS:
                        ser = new DataContractJsonSerializer(typeof(ShipsGameState));
                        break;
                }

                var output = string.Empty;
                if (ser == null)
                    return output;

                using (var ms = new MemoryStream())
                {
                    ser.WriteObject(ms, gameState);
                    ms.Position = 0;
                    using (var reader = new StreamReader(ms))
                    {
                        output = reader.ReadToEnd();
                    }
                }

                return output;
            }
            catch (Exception e)
            {
                log.Error("Error serializing GameState", e);
                return string.Empty;
            }
        }

        /// <summary>
        /// Deserialize JSON string into GameState object
        /// </summary>
        /// <param name="data"></param>
        /// <param name="gameType"></param>
        /// <returns></returns>
        private GameStateBase DeserializeGameStateFromJson(string data, string gameType)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    using (var writer = new StreamWriter(ms))
                    {
                        writer.Write(data);
                        writer.Flush();
                        ms.Position = 0;
                        if (gameType == GameType.SHIPS.ToString())
                        {
                            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ShipsGameState));
                            return (ShipsGameState)ser.ReadObject(ms);
                        }
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                log.Error("Error while deserializing GameState", e);
                return null;
            }
        }
    }
}
