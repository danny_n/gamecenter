﻿using System.Runtime.Serialization;

namespace GameCenter.Common.Enums
{
    [DataContract]
    public enum MoveResult
    {
        //COMMON
        [EnumMember]
        ERROR = 0,
        [EnumMember]
        GAME_OVER = 1,

        // SHIPS
        [EnumMember]
        WATER_HIT = 2,
        [EnumMember]
        SHIP_HIT = 3,
        [EnumMember]
        SHIP_DESTROYED = 4,

        //... add results for other games
    }
}
