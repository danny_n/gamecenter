﻿using System.Runtime.Serialization;

namespace GameCenter.Common.Enums
{
    [DataContract]
    public enum ResponseStatus
    {
        //REGISTER
        [EnumMember]
        REGISTER_SUCCESS,
        [EnumMember]
        REGISTER_FAILED,
        [EnumMember]
        REGISTER_ERROR,

        //LOGIN
        [EnumMember]
        LOGIN_SUCCESS,
        [EnumMember]
        LOGIN_FAILED,
        [EnumMember]
        LOGIN_ERROR,

        //MOVE
        [EnumMember]
        MOVE_SUCCESS,
        [EnumMember]
        MOVE_ERROR
    }
}
