﻿namespace GameCenter.Common.Utils
{
    public static class Extensions
    {
        /// <summary>
        /// Extension method for converting string to Int32. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertToInt(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return 0;

            int result;
            var success = int.TryParse(value, out result);
            if (success)
                return result;
            else
                return 0;
        }
    }
}
