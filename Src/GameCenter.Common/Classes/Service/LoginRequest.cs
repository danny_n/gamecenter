﻿using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Service
{
    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
