﻿using GameCenter.Common.Enums;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Service
{
    [DataContract]
    public class MoveResponse : ResponseBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        public MoveResponse(ResponseStatus status)
        {
            this.Status = status;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        public MoveResponse(ResponseStatus status, string message)
        {
            this.Status = status;
            this.Message = message;
        }

        /// <summary>
        /// Paramterless constructor
        /// </summary>
        public MoveResponse()
        {
        }
    }
}
