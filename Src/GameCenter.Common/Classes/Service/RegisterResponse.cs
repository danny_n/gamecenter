﻿using GameCenter.Common.Enums;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Service
{
    /// <summary>
    /// Class representing data for response after user register attempt
    /// </summary>
    [DataContract]
    public class RegisterResponse : ResponseBase
    {
        [DataMember]
        public int IdPlayer { get; set; }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        public RegisterResponse(ResponseStatus status, int idPlayer)
        {
            this.Status = status;
            this.IdPlayer = idPlayer;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        public RegisterResponse(ResponseStatus status, string message)
        {
            this.Status = status;
            this.Message = message;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public RegisterResponse()
        {
        }
    }
}
