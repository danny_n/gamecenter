﻿using GameCenter.Common.Enums;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Service
{
    /// <summary>
    /// Class representing data for response after user login attempt
    /// </summary>
    [DataContract]
    public class LoginResponse : ResponseBase
    {
        [DataMember]
        public int IdPlayer { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        public LoginResponse(ResponseStatus status, int idPlayer)
        {
            this.Status = status;
            this.IdPlayer = idPlayer;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        public LoginResponse(ResponseStatus status, string message)
        {
            this.Status = status;
            this.Message = message;
        }

        /// <summary>
        /// Paramterless contructor
        /// </summary>
        public LoginResponse()
        { }
    }
}
