﻿using System;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Service
{
    [DataContract]
    public class RegisterRequest
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
