﻿using GameCenter.Common.Classes.Base;
using GameCenter.Common.Enums;
using System.Runtime.Serialization;

namespace GameCenter.Service.Classes
{
    [DataContract]
    public class ResponseBase
    {
        [DataMember]
        public ResponseStatus Status { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public MoveResultBase MoveResult { get; set; }
    }
}
