﻿using GameCenter.Common.Enums;
using System.Runtime.Serialization;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace GameCenter.Common.Classes.Base
{
    [DataContract]
    //TODO possible fix for serialzing base classes
    //change solution structure to make ShipsGameState accessible
    //[KnownType(typeof(ShipsGameState))]
    public abstract class GameStateBase
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [DataMember]
        public int IdGame { get; set; }

        [DataMember]
        public GameType GameType { get; set; }

        public abstract MoveResultBase MakeMove(MoveBase move);
    }
}
