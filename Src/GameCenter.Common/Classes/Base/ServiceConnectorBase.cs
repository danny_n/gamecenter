﻿using GameCenter.Common.Classes.Service;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Base
{
    public abstract class ServiceConnectorBase
    {
        public abstract LoginResponse Login(string userName, string password);

        public abstract RegisterResponse Register(string userName, string password);

        public abstract int CreateGame(GameStateBase initialGameState);

        public abstract MoveResponse MakeMove(MoveBase move);
    }
}
