﻿using GameCenter.Common.Enums;
using System.Runtime.Serialization;

namespace GameCenter.Common.Classes.Base
{
    [DataContract]
    public abstract class MoveBase
    {
        [DataMember]
        public int IdPlayer { get; set; }
        [DataMember]
        public int IdGame { get; set; }
        [DataMember]
        public GameType GameType { get; set; }
        [DataMember]
        public string MoveFrom { get; set; }
        [DataMember]
        public string MoveTo { get; set; }
        [DataMember]
        public string MovingObject { get; set; }
    }
}
