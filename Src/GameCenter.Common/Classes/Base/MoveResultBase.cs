﻿using GameCenter.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GameCenter.Common.Classes.Base
{
    [DataContract]
    public class MoveResultBase
    {
        [DataMember]
        public GameStateBase CurrentState { get; set; }

        [DataMember]
        public MoveResult MoveResult { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool IsGameEnd { get; set; }
    }
}
