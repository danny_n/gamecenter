﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (4 + 0));
			InitGameEntityInfos();
			InitMoveEntityInfos();
			InitPlayerEntityInfos();
			InitPlayerGameEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits GameEntity's FieldInfo objects</summary>
		private void InitGameEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GameFieldIndex), "GameEntity");
			this.AddElementFieldInfo("GameEntity", "EndDate", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)GameFieldIndex.EndDate, 0, 0, 0);
			this.AddElementFieldInfo("GameEntity", "GameState", typeof(System.String), false, false, false, true,  (int)GameFieldIndex.GameState, 8000, 0, 0);
			this.AddElementFieldInfo("GameEntity", "GameStatus", typeof(System.String), false, false, false, false,  (int)GameFieldIndex.GameStatus, 16, 0, 0);
			this.AddElementFieldInfo("GameEntity", "GameType", typeof(System.String), false, false, false, false,  (int)GameFieldIndex.GameType, 16, 0, 0);
			this.AddElementFieldInfo("GameEntity", "IdGame", typeof(System.Int32), true, false, false, false,  (int)GameFieldIndex.IdGame, 0, 0, 10);
			this.AddElementFieldInfo("GameEntity", "IdPlayer", typeof(Nullable<System.Int32>), false, true, false, true,  (int)GameFieldIndex.IdPlayer, 0, 0, 10);
			this.AddElementFieldInfo("GameEntity", "StartDate", typeof(System.DateTime), false, false, false, false,  (int)GameFieldIndex.StartDate, 0, 0, 0);
		}
		/// <summary>Inits MoveEntity's FieldInfo objects</summary>
		private void InitMoveEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MoveFieldIndex), "MoveEntity");
			this.AddElementFieldInfo("MoveEntity", "IdGame", typeof(System.Int32), false, true, false, false,  (int)MoveFieldIndex.IdGame, 0, 0, 10);
			this.AddElementFieldInfo("MoveEntity", "IdMove", typeof(System.Int32), true, false, false, false,  (int)MoveFieldIndex.IdMove, 0, 0, 10);
			this.AddElementFieldInfo("MoveEntity", "IdPlayer", typeof(System.Int32), false, true, false, false,  (int)MoveFieldIndex.IdPlayer, 0, 0, 10);
			this.AddElementFieldInfo("MoveEntity", "MoveFrom", typeof(System.String), false, false, false, true,  (int)MoveFieldIndex.MoveFrom, 16, 0, 0);
			this.AddElementFieldInfo("MoveEntity", "MoveTo", typeof(System.String), false, false, false, false,  (int)MoveFieldIndex.MoveTo, 16, 0, 0);
			this.AddElementFieldInfo("MoveEntity", "MovingObject", typeof(System.String), false, false, false, true,  (int)MoveFieldIndex.MovingObject, 32, 0, 0);
		}
		/// <summary>Inits PlayerEntity's FieldInfo objects</summary>
		private void InitPlayerEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PlayerFieldIndex), "PlayerEntity");
			this.AddElementFieldInfo("PlayerEntity", "IdPlayer", typeof(System.Int32), true, false, false, false,  (int)PlayerFieldIndex.IdPlayer, 0, 0, 10);
			this.AddElementFieldInfo("PlayerEntity", "IsOnline", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)PlayerFieldIndex.IsOnline, 0, 0, 0);
			this.AddElementFieldInfo("PlayerEntity", "Password", typeof(System.String), false, false, false, false,  (int)PlayerFieldIndex.Password, 32, 0, 0);
			this.AddElementFieldInfo("PlayerEntity", "Username", typeof(System.String), false, false, false, false,  (int)PlayerFieldIndex.Username, 32, 0, 0);
		}
		/// <summary>Inits PlayerGameEntity's FieldInfo objects</summary>
		private void InitPlayerGameEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PlayerGameFieldIndex), "PlayerGameEntity");
			this.AddElementFieldInfo("PlayerGameEntity", "IdGame", typeof(System.Int32), true, true, false, false,  (int)PlayerGameFieldIndex.IdGame, 0, 0, 10);
			this.AddElementFieldInfo("PlayerGameEntity", "IdPlayer", typeof(System.Int32), true, true, false, false,  (int)PlayerGameFieldIndex.IdPlayer, 0, 0, 10);
		}
		
	}
}




