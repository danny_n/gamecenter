﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using GameCenterLLBL;
using GameCenterLLBL.FactoryClasses;
using GameCenterLLBL.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Move. </summary>
	public partial class MoveRelations
	{
		/// <summary>CTor</summary>
		public MoveRelations()
		{
		}

		/// <summary>Gets all relations of the MoveEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GameEntityUsingIdGame);
			toReturn.Add(this.PlayerEntityUsingIdPlayer);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MoveEntity and GameEntity over the m:1 relation they have, using the relation between the fields:
		/// Move.IdGame - Game.IdGame
		/// </summary>
		public virtual IEntityRelation GameEntityUsingIdGame
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Game", false);
				relation.AddEntityFieldPair(GameFields.IdGame, MoveFields.IdGame);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MoveEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MoveEntity and PlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// Move.IdPlayer - Player.IdPlayer
		/// </summary>
		public virtual IEntityRelation PlayerEntityUsingIdPlayer
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Player", false);
				relation.AddEntityFieldPair(PlayerFields.IdPlayer, MoveFields.IdPlayer);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MoveEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMoveRelations
	{
		internal static readonly IEntityRelation GameEntityUsingIdGameStatic = new MoveRelations().GameEntityUsingIdGame;
		internal static readonly IEntityRelation PlayerEntityUsingIdPlayerStatic = new MoveRelations().PlayerEntityUsingIdPlayer;

		/// <summary>CTor</summary>
		static StaticMoveRelations()
		{
		}
	}
}
