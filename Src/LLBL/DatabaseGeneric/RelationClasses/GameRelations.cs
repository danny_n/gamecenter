﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using GameCenterLLBL;
using GameCenterLLBL.FactoryClasses;
using GameCenterLLBL.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace GameCenterLLBL.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Game. </summary>
	public partial class GameRelations
	{
		/// <summary>CTor</summary>
		public GameRelations()
		{
		}

		/// <summary>Gets all relations of the GameEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MoveEntityUsingIdGame);
			toReturn.Add(this.PlayerGameEntityUsingIdGame);
			toReturn.Add(this.PlayerEntityUsingIdPlayer);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GameEntity and MoveEntity over the 1:n relation they have, using the relation between the fields:
		/// Game.IdGame - Move.IdGame
		/// </summary>
		public virtual IEntityRelation MoveEntityUsingIdGame
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Moves" , true);
				relation.AddEntityFieldPair(GameFields.IdGame, MoveFields.IdGame);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MoveEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GameEntity and PlayerGameEntity over the 1:n relation they have, using the relation between the fields:
		/// Game.IdGame - PlayerGame.IdGame
		/// </summary>
		public virtual IEntityRelation PlayerGameEntityUsingIdGame
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PlayerGames" , true);
				relation.AddEntityFieldPair(GameFields.IdGame, PlayerGameFields.IdGame);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerGameEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GameEntity and PlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// Game.IdPlayer - Player.IdPlayer
		/// </summary>
		public virtual IEntityRelation PlayerEntityUsingIdPlayer
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Player", false);
				relation.AddEntityFieldPair(PlayerFields.IdPlayer, GameFields.IdPlayer);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGameRelations
	{
		internal static readonly IEntityRelation MoveEntityUsingIdGameStatic = new GameRelations().MoveEntityUsingIdGame;
		internal static readonly IEntityRelation PlayerGameEntityUsingIdGameStatic = new GameRelations().PlayerGameEntityUsingIdGame;
		internal static readonly IEntityRelation PlayerEntityUsingIdPlayerStatic = new GameRelations().PlayerEntityUsingIdPlayer;

		/// <summary>CTor</summary>
		static StaticGameRelations()
		{
		}
	}
}
