﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.0
// Code is generated on: nedeľa, 18. decembra 2016 18:09:16
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace GameCenterLLBL
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Game.</summary>
	public enum GameFieldIndex
	{
		///<summary>EndDate. </summary>
		EndDate,
		///<summary>GameState. </summary>
		GameState,
		///<summary>GameStatus. </summary>
		GameStatus,
		///<summary>GameType. </summary>
		GameType,
		///<summary>IdGame. </summary>
		IdGame,
		///<summary>IdPlayer. </summary>
		IdPlayer,
		///<summary>StartDate. </summary>
		StartDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Move.</summary>
	public enum MoveFieldIndex
	{
		///<summary>IdGame. </summary>
		IdGame,
		///<summary>IdMove. </summary>
		IdMove,
		///<summary>IdPlayer. </summary>
		IdPlayer,
		///<summary>MoveFrom. </summary>
		MoveFrom,
		///<summary>MoveTo. </summary>
		MoveTo,
		///<summary>MovingObject. </summary>
		MovingObject,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Player.</summary>
	public enum PlayerFieldIndex
	{
		///<summary>IdPlayer. </summary>
		IdPlayer,
		///<summary>IsOnline. </summary>
		IsOnline,
		///<summary>Password. </summary>
		Password,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PlayerGame.</summary>
	public enum PlayerGameFieldIndex
	{
		///<summary>IdGame. </summary>
		IdGame,
		///<summary>IdPlayer. </summary>
		IdPlayer,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Game</summary>
		GameEntity,
		///<summary>Move</summary>
		MoveEntity,
		///<summary>Player</summary>
		PlayerEntity,
		///<summary>PlayerGame</summary>
		PlayerGameEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

