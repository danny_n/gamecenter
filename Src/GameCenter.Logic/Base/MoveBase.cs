﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCenter.Logic.Base
{
    public abstract class MoveBase
    {
        public int IdPlayer { get; set; }
        public int IdGame { get; set; }
        public string MoveFrom { get; set; }
        public string MoveTo { get; set; }
        public string MovingObject { get; set; }
    }
}
