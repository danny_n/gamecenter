﻿[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace GameCenter.Logic.Base
{
    public abstract class GameStateBase
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public abstract MoveResultBase MakeMove(MoveBase move);
    }
}
