﻿using GameCenter.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCenter.Logic.Base
{
    public class MoveResultBase
    {
        public GameStateBase CurrentState { get; set; }

        public MoveResult MoveResult { get; set; }

        public string Message { get; set; }
    }
}
