﻿using GameCenter.Common.Classes.Base;

namespace GameCenter.Logic.Ships
{
    public class ShipsMoveResult : MoveResultBase
    {
        public Ship DestroyedShip { get; set; }
    }
}
