﻿using GameCenter.Common.Classes.Base;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace GameCenter.Logic.Ships
{
    [DataContract]
    [KnownType(typeof(ShipsGameState))]
    public class ShipsGameState : GameStateBase
    {            
        [DataMember]
        public ShipsGameBoard Board_P1 { get; set; }
        [DataMember]
        public ShipsGameBoard Board_P2 { get; set; }
        
        /// <summary>
        /// Method apllies players move to the oponents board a calculates result.
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public override MoveResultBase MakeMove(MoveBase move)
        {
            var result = new ShipsMoveResult();

            try
            {
                // get oponents board
                var board = Board_P1.IdPlayer == move.IdPlayer ? Board_P2 : Board_P1;

                // get cell to attack
                var cell = board.Cells.Where(c => c.Matches(move.MoveTo)).FirstOrDefault();

                // mark cell as hit
                cell.IsHit = true;

                // determine hit impact (hit only or ship destruction)
                if (!cell.IsWater)
                {
                    result.MoveResult = Common.Enums.MoveResult.SHIP_HIT;

                    foreach (var ship in board.Ships)
                    {
                        if (!ship.IsDestroyed && ship.IsOnCell(cell))
                        {
                            ship.HitCount++;
                            if (ship.HitCount == (int)ship.Type)
                            {
                                ship.IsDestroyed = true;
                                result.MoveResult = Common.Enums.MoveResult.SHIP_DESTROYED;
                                result.DestroyedShip = ship;
                            }
                        }
                    }

                    // all ships are destroyed => GAME OVER
                    if (board.Ships.All(s => s.IsDestroyed))
                    {
                        result.IsGameEnd = true;
                        result.MoveResult = Common.Enums.MoveResult.GAME_OVER;
                    }
                }
                else
                {
                    result.MoveResult = Common.Enums.MoveResult.WATER_HIT;
                }

                result.CurrentState = this;
            }
            catch (Exception e)
            {
                result.MoveResult = Common.Enums.MoveResult.ERROR;
                result.Message = e.Message;
                log.Error("Error while MakeShipMove", e);
            }

            return result;
        }
    }
}