﻿using System.Runtime.Serialization;

namespace GameCenter.Logic.Ships
{
    [DataContract]
    public enum ShipType
    {
        [EnumMember]
        SUBMARINE = 1,
        [EnumMember]
        DESTROYER = 2,
        [EnumMember]
        CRUISER = 3,
        [EnumMember]
        BATTLESHIP = 4
    }
}
