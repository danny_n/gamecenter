﻿using System.Runtime.Serialization;

namespace GameCenter.Logic.Ships
{
    [DataContract]
    public class Ship
    {
        [DataMember]
        public ShipType Type { get; set; }

        [DataMember]
        public Cell Position { get; set; }

        [DataMember]
        public bool IsHorizontal { get; set; }
        [DataMember]
        public bool IsDestroyed { get; set; }
        [DataMember]
        public int HitCount { get; set; }

        /// <summary>
        /// Checks if the ships is places on given cell
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public bool IsOnCell(Cell cell)
        {
            if (IsHorizontal)
            {
                return (Position.X <= cell.X && Position.X + (int)Type >= cell.X);
            }
            else
            {
                return (Position.Y <= cell.Y && Position.Y + (int)Type >= cell.Y);
            }
        }
    }
}
