﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GameCenter.Logic.Ships
{
    [DataContract]
    public class ShipsGameBoard
    {
        [DataMember]
        public int IdPlayer { get; set; }
        [DataMember]
        public List<Cell> Cells { get; set; }
        [DataMember]
        public List<Ship> Ships { get; set; }

        /// <summary>
        /// Basic CTOR
        /// </summary>
        public ShipsGameBoard()
        { }

        /// <summary>
        /// CTOR with parameters
        /// </summary>
        /// <param name="idPlayer"></param>
        /// <param name="boardData"></param>
        public ShipsGameBoard(int idPlayer, int[][] boardData)
        {
            this.IdPlayer = idPlayer;

            char X_coor = 'A';
            int Y_coor = 1;
            this.Cells = new List<Cell>();

            if (boardData == null)
                return;

            foreach (var y_row in boardData)
            {
                foreach (var x in y_row)
                {
                    var c = new Cell();
                    c.X = X_coor;
                    c.Y = Y_coor;
                    c.IsWater = (x == 0);
                    c.IsHit = false;

                    this.Cells.Add(c);
                    X_coor++;
                }

                Y_coor++;
                X_coor = 'A';
            }

            // TODO fix implementation
            //this.Ships = FindShips(this.Cells);
        }

        /// <summary>
        /// Create list of Ships object from intial state
        /// </summary>
        /// <param name="cells"></param>
        /// <returns></returns>
        private List<Ship> FindShips(List<Cell> cells)
        {
            var result = new List<Ship>();
            if (!cells.Any())
                return result;

            foreach (var c in cells)
            {
                if (c.IsHit)
                    continue;

                if (c.IsWater)
                {
                    c.IsHit = true;
                    continue;
                }

                var s = FindShip(c, cells);
                result.Add(s);
            }

            return result;
        }

        /// <summary>
        /// Create a Ships
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        private Ship FindShip(Cell cell, List<Cell> cells)
        {
            Ship s = null;

            try
            {
                cell.IsHit = true;

                s = new Ship();
                s.Position = cell;

                int size = 1;
                // right
                var nextRight = cells.FirstOrDefault(c => c.Y == cell.Y && c.X == cell.X + 1 && c.IsWater == false);
                var nextLeft = cells.FirstOrDefault(c => c.Y == cell.Y && c.X == cell.X - 1 && c.IsWater == false);
                var nextUp = cells.FirstOrDefault(c => c.Y == cell.Y - 1 && c.X == cell.X && c.IsWater == false);
                var nextDown = cells.FirstOrDefault(c => c.Y == cell.Y + 1 && c.X == cell.X && c.IsWater == false);

                if (nextRight != null || nextRight != null)
                    s.IsHorizontal = true;
                else if (nextUp != null || nextDown != null)
                    s.IsHorizontal = false;

                if (nextRight != null)
                {
                    do
                    {
                        nextRight = cells.FirstOrDefault(c => c.Y == nextRight.Y && c.X == nextRight.X + 1 && c.IsWater == false);
                        if (nextRight != null)
                        {
                            nextRight.IsHit = true;
                            size++;
                        }
                    } while (nextRight != null);
                }
                else if (nextLeft != null)
                {
                    do
                    {
                        nextLeft = cells.FirstOrDefault(c => c.Y == cell.Y && c.X == cell.X - 1 && c.IsWater == false);
                        if (nextLeft != null)
                        {
                            nextLeft.IsHit = true;
                            size++;
                        }
                    } while (nextLeft != null);
                }
                else if (nextUp != null)
                {
                    do
                    {
                        nextUp = cells.FirstOrDefault(c => c.Y == cell.Y - 1 && c.X == cell.X && c.IsWater == false);
                        if (nextUp != null)
                        {
                            nextUp.IsHit = true;
                            size++;
                        }
                    } while (nextUp != null);
                }
                else if (nextDown != null)
                {
                    do
                    {
                        nextDown = cells.FirstOrDefault(c => c.Y == cell.Y + 1 && c.X == cell.X && c.IsWater == false);
                        if (nextDown != null)
                        {
                            nextDown.IsHit = true;
                            size++;
                        }
                    } while (nextDown != null);
                }

                s.Type = (ShipType)size;
            }
            catch
            {
                return null;
            }
            return s;
        }
    }
}
