﻿using System.Runtime.Serialization;

namespace GameCenter.Logic.Ships
{
    [DataContract]
    public class Cell
    {
        [DataMember]
        public char X { get; set; }
        [DataMember]
        public int Y { get; set; }

        [DataMember]
        public bool IsWater { get; set; }
        [DataMember]
        public bool IsHit { get; set; }



        /// <summary>
        /// Compares cell object against given string coordinates
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool Matches(string val)
        {
            if ($"{X}-{Y}" == val)
                return true;

            return false;
        }
    }
}
